package bankaccount;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double balance, percent;

        System.out.print("Введите баланс -  ");
        balance = input.nextDouble();
        System.out.print("Введите годовой процент -  ");
        percent = input.nextDouble();

        Account account = new Account(balance, percent);

        System.out.println(account);
        System.out.printf("\nБаланс с годовой ставкой через год - %5.2f RUB\n", account.calculateAnnualInterestCharge());
    }
}