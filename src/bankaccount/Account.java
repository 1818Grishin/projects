package bankaccount;

public class Account {
    private double balance;
    private double percent;

    public Account(double balance, double percent) {
        this.balance = balance;
        this.percent = percent;
    }

    public double calculateAnnualInterestCharge() {
        return balance * 0.01 * percent + balance;
    }

    public String toString() {
        return "\nДанные аккаунта: \nБаланс -  " + balance + " руб." +
                "\nПроцент -  " + percent + " %";
    }
}