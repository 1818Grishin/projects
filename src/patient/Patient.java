package patient;

public class Patient {
    private String surname;
    private int birthYear;
    private int cardNumber;
    private boolean medExam;

    public Patient (String surname, int birthYear, int cardNumber, boolean medExam) {
        this.surname = surname;
        this.birthYear = birthYear;
        this.cardNumber = cardNumber;
        this.medExam = medExam;
    }

    public Patient () {
        this("Не указана", 0, 0, false);
    }

    public String getSurname() {
        return surname;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public boolean isMedExam() {
        return medExam;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "surname='" + surname + '\'' +
                ", birthYear=" + birthYear +
                ", cardNumber=" + cardNumber +
                ", medExam=" + medExam +
                '}';
    }
}
