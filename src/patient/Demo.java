package patient;

public class Demo {
    public static void main(String[] args){
        Patient[] patients = new Patient[5];

        new Patient("Смирнов", 2002, 1, false);
        new Patient("Иванов", 1999, 2, true);
        new Patient("*******", 1975, 37404, true);
        new Patient("Крюков", 2004, 32, true);
        new Patient("Семёнов", 1952, 4, false);

    }
}
