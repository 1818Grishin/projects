package baggage;

import java.util.ArrayList;

import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Сколько пассажиров вы хотите ввести: ");
        int num = scanner.nextInt();
        ArrayList<Baggage> baggages = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            baggages.add(input());
        }
        pasClad(baggages);
    }

    private static Baggage input() {
        System.out.print("Введите фамилию: ");
        scanner.nextLine();
        String surname = scanner.nextLine();
        System.out.print("Введите количество багажных мест: ");
        int number = scanner.nextInt();
        System.out.print("Общий вес багажа: ");
        double weight = scanner.nextDouble();
        return new Baggage(surname, number, weight);
    }

    private static void pasClad(ArrayList<Baggage> baggages) {
        int baggagesSize = baggages.size();
        for (int i = 0; i < baggagesSize; i++) {
            if (baggages.get(i).Сlad()) {
                System.out.println(baggages.get(i).getSurname() + " с ручной кладью");
            }
        }
    }
}