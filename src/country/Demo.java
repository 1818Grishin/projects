package country;

import java.util.ArrayList;

import java.util.Scanner;

public class Demo {
    private static Scanner read = new Scanner(System.in);
    public static void main(String[] args) {
        Country russia = new Country("Россия","Москва",300,500);
        System.out.print("Сколько стран вы хотите ввести: ");
        int num = read.nextInt();
        ArrayList <Country> countries = new ArrayList<>();
        for (int i = 0; i < num ; i++) {
            countries.add(input());
        }
        countries.add(russia);
        System.out.println("Самая населенная страна: " + countries.get(bigPopulation(countries)));
    }

    private static Country input(){
        System.out.print("Введите название: ");
        read.nextLine();
        String name = read.nextLine();
        System.out.print("Введите столицу: ");
        String capital = read.nextLine();
        System.out.print("Введите площадь: ");
        double area = read.nextDouble();
        System.out.print("Введите навеление:");
        int population = read.nextInt();
        return new Country(name,capital,area,population);
    }

    private static int bigPopulation (ArrayList <Country> countries){
        int countriesSize = countries.size();
        double max = 0;
        int part = 0;

        for (int i = 0; i < countriesSize; i++) {
            double bigPopulation = countries.get(i).bigPopulation();
            if (max < bigPopulation) {
                part = i;
                max = bigPopulation;
            }
        }
        return part;
    }
}