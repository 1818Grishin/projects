package refactoring;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Введите число - ");

        System.out.println("Преобразование в двоичное - " + Representation.representAsBinary(input.nextDouble()));
    }
}
