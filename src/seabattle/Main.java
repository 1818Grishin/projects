package seabattle;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int field = (int) (Math.random() * 7 + 4);
        int shipLength = (int) (Math.random() * 4 + 1);
        int startCoord = (int) (Math.random() * (field - shipLength + 1) + 1);
        int coord;
        String result;

        Ship ship = new Ship(startCoord, shipLength);

        System.out.println("Размер поля - " + field + "\nРазмер корабля - " + shipLength);

        do {
            System.out.print("Введите координаты - ");
            coord = input.nextInt();

            result = ship.shoot(coord);
            System.out.println(result);
        }
        while(!result.equals("Корабль потоплен"));
    }
}