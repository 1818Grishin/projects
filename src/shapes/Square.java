package shapes;

public class Square extends Shape{
    private double side;
    private Point corner;

    public Square(Color color, double side, Point corner) {
        super(color);
        this.side = side;
        this.corner = corner;
    }

    public double area() {
        return side*side;
    }

    @Override
    public String toString() {
        return "Square{" +
                "side=" + side +
                ", corner=" + corner +
                '}';
    }
}