package temperature;

import java.util.Scanner;

public class Temperature {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int numberOfDays;
        System.out.print("Введите количество дней:");
        numberOfDays = scanner.nextInt();
        double temperature[] = new double [numberOfDays];
        fillTemperature(temperature);
        System.out.println("");
        System.out.printf("Средняя температура: %d", averageTemperature(temperature));
        System.out.println("");
        double max = getMax(temperature);
        System.out.printf("Маскимальная температура: %5.1f ", max);
        System.out.println("");
        double min = getMin(temperature);
        System.out.printf("Минимальная температура: %5.1f ", min);
        System.out.println("");
        сoldday(temperature,min);
        warmday(temperature,max);
    }

    private static void сoldday(double[] temperature,double min) {
        int number[] = new int[30];
        int x = 0;
        for (int i = 0; i < temperature.length; i++) {
            if (min == temperature[i]) {
                number[x] = i + 1;
                System.out.printf("Холодные дни: %d   ",number[x]);
                x++;
            }
        }
    }

    private static void warmday(double[] temperature,double max) {
        int number[] = new int[30];
        int x = 0;
        for (int i = 0; i < temperature.length; i++) {
            if (max == temperature[i]) {
                number[x] = i + 1;
                System.out.printf("   Теплые дни: %d   ",number[x]);
                x++;
            }
        }
    }

    private static void fillTemperature(double[] temperature) {
        System.out.println("Температура дней: ");
        for (int i = 0; i < temperature.length; i++) {
            temperature[i] = (-20 + (Math.random() * 50));
            System.out.printf("%5.1f", temperature[i]);
            System.out.println(" ");
        }
    }

    private static double getMin(double temperature[]) {
        double minValue = temperature[0];
        for (int i = 1; i < temperature.length; i++) {
            if (temperature[i] < minValue) {
                minValue = temperature[i];
            }
        }
        return minValue;
    }

    private static double getMax(double temperature[]){
        double maxValue = temperature[0];
        for(int i=1;i < temperature.length;i++){
            if( temperature[i] > maxValue){
                maxValue = temperature[i];
            }
        }
        return maxValue;
    }

    private static int averageTemperature(double temperature[]) {
        double sum =0.0;
        for (double temperatureOfDay : temperature) {
            sum += temperatureOfDay;
        }
        return (int) (sum / temperature.length);
    }
}