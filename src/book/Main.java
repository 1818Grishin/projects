package book;
import java.util.Scanner;
import java.util.ArrayList;


public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Book book1 = new Book("Толстой", "Война и мир", 2015);
        Book book2 = new Book("Пушкни", "Евгений Онегин", 2020);
        Book book3 = new Book("Куприн", "Чудесный доктор", 2013);
        Book book4 = new Book("Толстой", "Анна Каренина", 2009);
        Book book5 = new Book("Гомер", "Илиада", 2018);

        ArrayList<Book> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);
        books.add(book3);
        books.add(book4);
        books.add(book5);
        books.add(input());
        printMessageBooks(books);
        printComparison(books);
    }

    private static Book input() {
        System.out.println("Введите автора: ");
        String surname = scanner.nextLine();
        System.out.println("Введите название книги: ");
        String name = scanner.nextLine();
        System.out.println("Введите год издания книги: ");
        int year = scanner.nextInt();
        return new Book(surname, name, year);
    }

    private static void printMessageBooks(ArrayList<Book> books) {
        int booksSize = books.size();
        if (books.get(0).isYears(books.get(booksSize - 1))) {
            System.out.println("Год издания книги - " + books.get(0).getName() + "- и книги - " + books.get(booksSize - 1).getName() + " - равны ");
        } else {
            System.out.println("Год издания книги - " + books.get(0).getName() + "- и книги - " + books.get(booksSize - 1).getName() + " - не равны");
        }
    }

    private static void printComparison(ArrayList<Book> books) {
        for (Book book : books) {
            if (book.getYear() == 2018) {
                System.out.println("Книга - " + book.getName() + " - 2018-го года издания \n" + book);

            }
        }
    }
}