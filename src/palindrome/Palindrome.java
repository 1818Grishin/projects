package palindrome;
import java.util.Scanner;

public class Palindrome {
    public static void main (String[] args){
        Scanner read = new Scanner(System.in);
        String sentence;

        System.out.println("Введите предложение - ");
        sentence = read.nextLine();

        if (sentence.toLowerCase().equals((new StringBuffer(sentence).reverse().toString().toLowerCase()))){
            System.out.print("Введенное предложение является палиндромом.");
        }
        else System.out.print("Введенное предложение не является палиндромом.");
    }
}
